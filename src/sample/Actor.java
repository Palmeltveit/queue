package sample;

import javafx.collections.ObservableList;

/**
 * Created by Pål on 08.06.2017.
 */
public class Actor {

    private String name;
    private ObservableList<RowKeeper> rows;

    public Actor(String name){
        this.name = name;
    }

    public ObservableList<RowKeeper> getRows(){
        return rows;
    }

    public String toString(){
        return name;
    }
}
