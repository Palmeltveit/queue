package sample.controllers;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.Appointment;
import sample.controllers.Controller;
import sample.custom_controls.CustomCell;

import java.net.URL;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * Created by Pål on 18.05.2017.
 */
public class FormControl implements Initializable {

    private int DEFAULT_MEET_TIME_MINUTES; //minutes
    private int duration; //minutes

    private Controller mainControl;

    private int selectedRow;
    private int appointmentIndex;

    private Appointment appointment;

    public ChoiceBox actorList;
    public Label timePicked;
    public Label cellsPicked;
    public TextField name;
    public TextField id;
    public TextArea description;
    public DatePicker date;

    private Stage stage;

    public LocalTime chosenTime;

    public FormControl(Controller mainControl, int rowClickedNum, int appointmentIndex, int defaultMinutes, int cellCount) {

        this.mainControl = mainControl;

        this.DEFAULT_MEET_TIME_MINUTES = defaultMinutes;
        this.duration = cellCount*defaultMinutes;

        this.selectedRow = rowClickedNum;
        this.chosenTime = mainControl.getRows().get(selectedRow).getTime().minusHours(0); //Easy way of cloning the LocalTime

        this.appointmentIndex = appointmentIndex - 1;
        this.appointment = mainControl.getRows().get(selectedRow).getAppointments().get(this.appointmentIndex);

        openWindow();
    }

    private void openWindow(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                    "appointmentForm.fxml"));
            fxmlLoader.setController(this);

            BorderPane mainContent = fxmlLoader.load();

            stage = new Stage();

            stage.setTitle("Add appointment");
            stage.setScene(new Scene(mainContent, 500, 300));
            stage.setAlwaysOnTop(true);

            stage.show();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.timePicked.setText(chosenTime.toString());

        actorList.getItems().addAll(mainControl.getActorList().getItems());
        actorList.getSelectionModel().select(mainControl.getActorList().getValue());

        cellsPicked.setText("" + duration/DEFAULT_MEET_TIME_MINUTES);

        //Pre-selecting the name textbox
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                name.requestFocus();
            }
        });

        updateForm();
    }

    public void updateForm(){
        if(date.getValue() == null) {
            date.setValue(mainControl.getRows().get(selectedRow).getDate(appointmentIndex));
        }
        if(appointment != null) {
            name.setText(appointment.getName());
            description.setText(appointment.getDescription());
        } else {
            name.setText("");
            description.setText("");
        }
    }

    public void toggleActor(){
        mainControl.getActorList().getSelectionModel().select(this.actorList.getValue());
        appointment = mainControl.getRows().get(selectedRow).getAppointments().get(appointmentIndex);

        updateForm();
    }

    public void toggleDate(){
        mainControl.setDate(date.getValue());
        appointment = mainControl.getRows().get(selectedRow).getAppointments().get(appointmentIndex);

        updateForm();
    }

    public void increase(){
        changeTime(DEFAULT_MEET_TIME_MINUTES);
    }

    public void decrease(){
        changeTime(-DEFAULT_MEET_TIME_MINUTES);
    }

    private void changeTime(int minutes){
        if((minutes > 0 && selectedRow < mainControl.getRows().size()-1)|| (minutes < 0 && selectedRow > 0)) {
            chosenTime = chosenTime.plusMinutes(minutes);
            timePicked.setText(chosenTime.toString());

            if(minutes > 0) {
                selectedRow ++;
            } else {
                selectedRow --;
            }
            appointment = mainControl.getRows().get(selectedRow).getAppointments().get(appointmentIndex);
            updateForm();
        }
    }

    public void submit(){
        TablePosition<Integer, Integer> pos = (TablePosition) mainControl.table.getSelectionModel().getSelectedCells().get(0);

        if(appointment != null && !name.getText().equals("")) {
            try {
                Appointment newApt = mainControl.getDbHandler().editAppointment(mainControl, name.getText(), appointment.getDbID());

                mainControl.getRows().get(pos.getRow()).getAppointments().set(pos.getColumn()-1, newApt);
                mainControl.table.refresh();
            } catch (SQLException s){
                s.printStackTrace();
            }
        } else if(!name.getText().equals("")) {
            try {
                Appointment newApt = mainControl.getDbHandler().addAppointment(mainControl, date.getValue(), Time.valueOf(chosenTime), duration, name.getText(), actorList.getValue().toString());
                //mainControl.getRows().get(pos.getRow()).getAppointments().set(pos.getColumn()-1, newApt);
                mainControl.putIntoRowKeeper(newApt);
                mainControl.table.refresh();

            } catch (SQLException e) {
                //should react to these exceptions in the future to prevent possible crashes
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Alert notSavedAlert = new Alert(Alert.AlertType.INFORMATION, "The appointment was not saved");
            notSavedAlert.show();
        }
        stage.close();
    }

    public void cancel(){
        mainControl.table.refresh();
        close();
    }

    public void close(){
        stage.close();
    }
}
