package sample.controllers;

import sample.controllers.Controller;
import sample.controllers.SettingsControl;

/**
 * Created by Pål on 20.05.2017.
 */
public class MenuBarController {

    private SettingsControl settingsControl;
    private Controller mainControl;

    public void openSettings(){

        if (settingsControl != null) {
            settingsControl.reOpen();
        } else {
            this.settingsControl = new SettingsControl(mainControl);
        }
    }

    public void addDBHandler(Controller main){
        this.mainControl = main;
    }

}
