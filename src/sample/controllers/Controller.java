package sample.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import sample.*;
import sample.custom_controls.*;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

public class Controller implements Initializable {

    //COLUMN RELATED VARIABLES
    private static final int COLUMNS_MAX = 5;
    private static final int COLUMNS_MIN = 1;

    //VARIABLES FOR LOGGING DATABASE-CHANGES AND MODES
    private int dbTimeSettings = 0; //0 is always start values

    private ArrayList<Mode> modes = new ArrayList<>();

    //START- AND END-TIME + DEF. MEET TIME
    private ArrayList<TimeSettings> timeSettings = new ArrayList<>();
    private TimeSettings sumTimeSettings; //holds the first start and last end;
    private int meetTimeMinutes; //minutes


    //various arrayLists for table structure and stuff
    private ArrayList<LocalDate> days = new ArrayList<>();
    private ArrayList<LocalTime> appointmentTimes = new ArrayList<>();

    private int DEFAULT_TIME_COL_WIDTH = 50;

    private int numberOfColumns = 1;
    private int columnWidth = 300;
    private TableColumn timeCol;

    private ObservableList<RowKeeper> rows;
    private ObservableList<String>  actors;

    private final ContextMenu tableMenu = new ContextMenu();

    //fxml objects
    @FXML public ChoiceBox actorList;
    @FXML public NumberTextField columnCount;
    @FXML public ProCalendar proCal;
    @FXML public CustomTable table;

    //Database handler
    private DBHandler dbHandler;
    private LocalDate date;
    private FormControl newAppointmentController;

    private boolean hasStarted = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        dbHandler = new DBHandler();

        table.setMainControl(this);
        table.getSelectionModel().setCellSelectionEnabled(true);

        table.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if(oldValue.intValue() < newValue.intValue()) {
                updateColumnWidth();
            } else {
                int diff = oldValue.intValue() - newValue.intValue();
                if(diff > 50){
                    updateTable();
                }
            }
        });

        columnCount.setText("" + numberOfColumns);

        getSettings();
        fetchActors();
        getDate(); //getting default proCal value, today's date


        //initializing table context (right-click) menu
        initializeTableContextMenu();
    }

    public void initializeTableContextMenu(){

        MenuItem autoFiller = new MenuItem("Autofill cell");
        autoFiller.setOnAction((ActionEvent e) -> {
            autoFillCell();
        });

        MenuItem open = new MenuItem("Open/Edit");
        open.setOnAction((ActionEvent e) -> {
            openTableForm(1);
        });

        MenuItem delete = new MenuItem("Delete appointment");
        delete.setOnAction((javafx.event.ActionEvent e) -> {
            deleteAppointment();
        });

        tableMenu.getItems().addAll(open, autoFiller, delete);
        table.setContextMenu(tableMenu);
    }

    public void fetchActors(){
        try {
            ResultSet result = dbHandler.getActors();

            actors = FXCollections.observableArrayList();

            while (result.next()) {
                actors.add(result.getString("actorName"));
            }

            actorList.setItems(actors);
            actorList.getSelectionModel().selectFirst();
            actorList.setOnAction(event -> {
                updateTable();
            });
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (SQLException s){
            s.printStackTrace();
            throw new RuntimeException("There was a problem fetching staff from database.");
        }
    }

    public void getDate(){
        date = proCal.picker.getValue();
        updateTable();
    }

    public void setDate(LocalDate newDate){
        date = newDate;
        proCal.setValue(newDate);
        updateTable();
    }

    public ChoiceBox getActorList(){
        return actorList;
    }

    public DBHandler getDbHandler() {
        return dbHandler;
    }

    //TABLE COLUMN MANAGEMENT
    public void increase(){
        changeColumnNum(numberOfColumns+1);
    }

    public void decrease(){
        changeColumnNum(numberOfColumns-1);
    }

    public void toggleColumnNum(){
        changeColumnNum(Integer.parseInt(columnCount.getText()));
    }

    public ObservableList<RowKeeper> getRows() {
        return rows;
    }

    private void changeColumnNum(int newNum){
        if(newNum >= COLUMNS_MIN && newNum <= COLUMNS_MAX) {
            this.numberOfColumns = newNum;
            columnCount.setText("" + numberOfColumns);

            updateTable();
        }
    }

    private void getSettings(){
        timeSettings.clear();
        modes.clear();

        sumTimeSettings = new TimeSettings(1, 7, LocalTime.MAX, LocalTime.MIN, 0);
        try {
            ResultSet timeRes = dbHandler.getSettings();
            while(timeRes.next()) {

                TimeSettings settings = new TimeSettings(timeRes.getInt("startDay"), timeRes.getInt("endDay"),
                        LocalTime.parse(timeRes.getString("startTime")), LocalTime.parse(timeRes.getString("endTime")), timeRes.getInt("id"));
                timeSettings.add(settings);
                System.out.println(settings + " -- " + settings.getStartDay() + ", " + settings.getEndDay());

                //assigning the earliest start time and latest end time from the (potentially multiple)
                //stored settings to the main settings holder, for better time-column generating
                if(settings.getStart().isBefore(sumTimeSettings.getStart())){
                    sumTimeSettings.setStart(settings.getStart());
                }

                if (settings.getEnd().isAfter(sumTimeSettings.getEnd())){
                    sumTimeSettings.setEnd(settings.getEnd());
                }
            }

            ResultSet modeRes = dbHandler.getModes();
            while(modeRes.next()){

                Mode mode = new Mode(modeRes.getInt("defaultMeetTime"), modeRes.getInt("styleMode"), modeRes.getInt("id"));
                modes.add(mode);
            }
            //TODO: should be done better, with regard to different modes
            if(meetTimeMinutes != modes.get(0).getDefaultMeetTime()) {
                this.meetTimeMinutes = modes.get(0).getDefaultMeetTime();
                appointmentTimes.clear();
            }

        } catch (ClassNotFoundException c){
            c.printStackTrace();
        } catch (SQLException s){
            s.printStackTrace();
            throw new RuntimeException("There was a problem fetching settings from database.");
        }
    }

    public TimeSettings getTimeSettingsFor(int day){
        for(TimeSettings settings : timeSettings){
            for(int i = settings.getStartDay(); i <= settings.getEndDay(); i++) {
                if (i == day) {
                    return settings;
                }
            }
        }
        return new TimeSettings(0, 0, LocalTime.MAX, LocalTime.MIN, 1);
    }


    //REACTING TO TABLE
    @FXML
    public void tableReactor(MouseEvent event) {
        if (event.getClickCount() == 2){ //Checking double click
            openTableForm(1);
        }
    }
    @FXML
    public void tableKeyReactor(javafx.scene.input.KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)){ //Checking if enter was clicked
            openTableForm(1);
        }
    }

    public void updateColumnWidth(){
        columnWidth = (int)(table.getWidth()-DEFAULT_TIME_COL_WIDTH)/numberOfColumns;

        ObservableList<TableColumn> tableColumns = table.getColumns();

        for(int i = 1; i < tableColumns.size(); i++){
            TableColumn column = tableColumns.get(i);
            column.setMinWidth(columnWidth);
        }
    }

    //UPDATING TABLE
    public void updateTable(){

        columnWidth = (int)(table.getWidth()-DEFAULT_TIME_COL_WIDTH)/numberOfColumns;

        table.getColumns().clear();
        table.getItems().clear();

        try {
            days = getDaySpan(date);

            rows = FXCollections.observableArrayList();
            makeTimeColumns(rows);

            for(LocalDate dat: days) {

                for(RowKeeper row: rows){
                    row.addDate(days.indexOf(dat), dat);
                    TimeSettings daySchedule = getTimeSettingsFor(dat.getDayOfWeek().getValue());
                    //Deactivating cells that correspond with wrong appointment times
                    if(row.getTime().isBefore(daySchedule.getStart()) || row.getTime().isAfter(daySchedule.getEnd())){
                        row.setInactive(days.indexOf(dat));
                    }
                }

                CustomColumn col = new CustomColumn();

                col.setText(dat.toString());
                col.setMinWidth(columnWidth);

                ResultSet result = dbHandler.getDate(dat, actorList.getValue().toString());

                while (result.next()) {
                    Appointment appointment = new Appointment(this, result.getInt("id"), result.getInt("duration"), result.getString("date"),
                            result.getString("time"),
                            result.getString("name"), "empty description");

                    putIntoRowKeeper(appointment);
                }
                col.setCellValueFactory(
                        new PropertyValueFactory<RowKeeper, String>("appointment" + (days.indexOf(dat)+1))
                );


                col.setCellFactory(new Callback<CustomColumn, CustomCell>() {
                    @Override
                    public CustomCell call(CustomColumn tableColumn) {
                        CustomCell cell = new CustomCell(table);
                        tableColumn.addCell(cell);
                        return cell;
                    }
                });

                table.getColumns().add(col);
            }

            table.setItems(rows);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void openTableForm(int cellCount) {
        RowKeeper keeper = (RowKeeper) table.getSelectionModel().getSelectedItem();
        if(keeper!=null) {
            TablePosition<Integer, Integer> pos = (TablePosition) table.getSelectionModel().getSelectedCells().get(0);

            if (pos.getColumn() > 0 && keeper.getDefaultReturnVal()[pos.getColumn() - 1]) {
                if (newAppointmentController != null) {
                    newAppointmentController.close();
                    newAppointmentController = null;
                }
                newAppointmentController = new FormControl(this, rows.indexOf(keeper), pos.getColumn(), meetTimeMinutes, cellCount);
            }
        }
    }

    public void openTableForm(int cellCount, int chosenCell){
        RowKeeper keeper = (RowKeeper) table.getSelectionModel().getSelectedItem();
        if(keeper!=null) {
            TablePosition<Integer, Integer> pos = (TablePosition) table.getSelectionModel().getSelectedCells().get(0);

            if (pos.getColumn() > 0 && keeper.getDefaultReturnVal()[pos.getColumn() - 1]) {
                if (newAppointmentController != null) {
                    newAppointmentController.close();
                    newAppointmentController = null;
                }
                newAppointmentController = new FormControl(this, chosenCell, pos.getColumn(), meetTimeMinutes, cellCount);
            }
        }
    }

    public void autoFillCell(){
        RowKeeper keeper = (RowKeeper) table.getSelectionModel().getSelectedItem();
        if(keeper!=null) {
            TablePosition<Integer, Integer> pos = (TablePosition) table.getSelectionModel().getSelectedCells().get(0);

            if (pos.getColumn() > 0 && keeper.getDefaultReturnVal()[pos.getColumn() - 1]) {
                if(keeper.getAppointments().get(pos.getColumn()-1) == null) {
                    LocalTime chosenTime = rows.get(rows.indexOf(keeper)).getTime().minusHours(0); //Easy way of cloning the LocalTime
                    try {
                        Appointment newApt = dbHandler.addAppointment(this, keeper.getDate(pos.getColumn()-1), Time.valueOf(chosenTime), 30, "RESERVED", actorList.getValue().toString());
                        //rows.get(pos.getRow()).getAppointments().set(pos.getColumn()-1, newApt);
                        putIntoRowKeeper(newApt);
                        table.refresh();
                        //updateTable();
                    } catch (SQLException e) {
                        //should react to these exceptions in the future to prevent possible crashes
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void deleteAppointment(){
        RowKeeper keeper = (RowKeeper) table.getSelectionModel().getSelectedItem();
        if(keeper!=null) {
            TablePosition<Integer, Integer> pos = (TablePosition) table.getSelectionModel().getSelectedCells().get(0);
            if (pos.getColumn() > 0 && keeper.getDefaultReturnVal()[pos.getColumn() - 1]) {
                Appointment appointment = keeper.getAppointments().get(pos.getColumn()-1);
                if(appointment != null){
                    try {
                        System.out.println(appointment.getDbID());
                        dbHandler.deleteAppointment(appointment.getDbID());
                        rows.get(pos.getRow()).getAppointments().set(pos.getColumn()-1, null);
                        table.refresh();
                    } catch (SQLException e) {
                        //should react to these exceptions in the future to prevent possible crashes
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private int getIndexOf(LocalTime time){
        /* this method is used to find the index of an appointment
        int the rows-array (which is ordered by time from earliest to latest)
        by calculating from START using DEFAULT_TIME_COL_WIDTH.
         */

        if(time.isBefore(sumTimeSettings.getStart().plusMinutes(1)) || time.isAfter(sumTimeSettings.getEnd())){
            return 0;
        }

        time = time.minusHours(sumTimeSettings.getStart().getHour()).minusMinutes(sumTimeSettings.getStart().getMinute());

        int index = 1;
        while (time.isAfter(LocalTime.MIN.plusMinutes(meetTimeMinutes))){
            time = time.minusMinutes(meetTimeMinutes);
            index ++;
        }
        return index;
    }

    private void fillTimeColumn(){

        LocalTime time = LocalTime.of(sumTimeSettings.getStart().getHour(), sumTimeSettings.getStart().getMinute()).minusMinutes(meetTimeMinutes);

        while (time.plusMinutes(meetTimeMinutes).isBefore(sumTimeSettings.getEnd())) {
            appointmentTimes.add(time.plusMinutes(meetTimeMinutes));
            time = time.plusMinutes(meetTimeMinutes);
        }
    }

    public void reloadSettings(){
        getSettings();
        this.timeCol = null;
        this.appointmentTimes = new ArrayList<>();
        updateTable();
    }

    private void makeTimeColumns(ObservableList<RowKeeper> rows){
        if(timeCol == null) {
            timeCol = new TableColumn();
            timeCol.setText("Time");
            timeCol.setMaxWidth(DEFAULT_TIME_COL_WIDTH);

            if (appointmentTimes.isEmpty()) {
                fillTimeColumn();
            }

            timeCol.setCellValueFactory(
                    new PropertyValueFactory<RowKeeper, String>("time")
            );
        }
        for (LocalTime time : appointmentTimes) {
            rows.add(new RowKeeper(time));
        }

        table.getColumns().add(timeCol);
    }

    private ArrayList<LocalDate> getDaySpan(LocalDate date) throws ParseException {
        ArrayList<LocalDate> days = new ArrayList<>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date nDate = format.parse(date.toString());

        Calendar c = Calendar.getInstance();
        c.setTime(nDate);

        int start = -(int)Math.floor((numberOfColumns-1)/2);
        int end =  start + numberOfColumns;

        for(int i = start; i < end; i++){

            c.add(Calendar.DATE, i);  // number of days to add
            LocalDate newDate = c.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            days.add(newDate);

            c.add(Calendar.DATE, -i);
        }

        return days;
    }

    //RECT STUFF
    public void makePreviewRect(MouseEvent event){
        table.startFullDrag();
    }

    public void fineTuneRect(MouseEvent event){
        System.out.println("a");
    }

    public void finishRectMakery(MouseEvent event){
        hasStarted = false;
        System.out.println("Rect done! Sick in the head!");
    }

    public TimeSettings getSumTimeSettings(){
        return sumTimeSettings;
    }

    public int getDefaultMeetTime() {
        return meetTimeMinutes;
    }

    public ArrayList<TimeSettings> getTimeSettings() {
        return timeSettings;
    }

    public ArrayList<Mode> getModes() {
        return modes;
    }

    public ObservableList<String> getActors() {
        return actors;
    }

    public void putIntoRowKeeper(Appointment appointment){
        int correspondingRowIndex = getIndexOf(appointment.getTime());
        LocalTime appointmentStartTime = appointment.getTime().minusMinutes(meetTimeMinutes);

        while(appointmentStartTime.plusMinutes(meetTimeMinutes).isBefore(appointment.getTime().plusMinutes(appointment.getDuration()))){
            rows.get(correspondingRowIndex).addAppointment(days.indexOf(appointment.getDate()), appointment, false);

            appointmentStartTime = appointmentStartTime.plusMinutes(meetTimeMinutes);
            correspondingRowIndex ++;
        }
    }
}
