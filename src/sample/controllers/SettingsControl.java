package sample.controllers;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.controllers.Controller;
import sample.TimeSettings;
import sample.custom_controls.NumberTextField;
import sample.custom_controls.TimeTextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * Created by Pål on 18.05.2017.
 */
public class SettingsControl implements Initializable {

    //TIME-SETTINGS RELATED
    public TimeTextField startTime;
    public TimeTextField endTime;
    public NumberTextField DMT;
    public ChoiceBox startDay;
    public ChoiceBox endDay;

    //ACTOR-SETTINGS RELATED
    public VBox actorList;

    private Stage stage;
    private BorderPane mainContent;
    private Controller mainControl;

    private String[] dayList = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public SettingsControl(Controller main) {
        this.mainControl = main;
        openWindow();
    }

    private void openWindow(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                    "settings.fxml"));
            fxmlLoader.setController(this);
            mainContent = fxmlLoader.load();

            stage = new Stage();
            stage.setTitle("Settings");
            stage.setScene(new Scene(mainContent, 750, 450));
            stage.show();

        } catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    //ACTOR SETTINGS RELATED
    public void setActorCenter(){

        try {
            FXMLLoader actorLoader = new FXMLLoader(getClass().getResource("actorSettings.fxml"));
            actorLoader.setController(this);
            VBox centerContent = actorLoader.load();

            DMT.setHighestNumber(9);
            DMT.setMaxLength(3);

            startDay.getItems().addAll(dayList);
            endDay.getItems().addAll(dayList);
            startDay.getSelectionModel().selectFirst();
            endDay.getSelectionModel().selectLast();

            for(String actor: mainControl.getActors()){
                actorList.getChildren().add(new Label(actor));
            }

            mainContent.setCenter(centerContent);
        } catch (IOException i){
            i.printStackTrace();
        }
    }



    //TIME SETTINGS RELATED
    public void setTimeCenter(){

        try {
            FXMLLoader timeLoader = new FXMLLoader(getClass().getResource("timeSettings.fxml"));
            timeLoader.setController(this);
            VBox centerContent = timeLoader.load();

            DMT.setHighestNumber(9);
            DMT.setMaxLength(3);

            startDay.getItems().addAll(dayList);
            endDay.getItems().addAll(dayList);
            startDay.getSelectionModel().selectFirst();
            endDay.getSelectionModel().selectLast();

            mainContent.setCenter(centerContent);
        } catch (IOException i){
            i.printStackTrace();
        }
    }

    public void saveMeetTime(){
        if(DMT.getText().length() > 0) {
            try {
                mainControl.getDbHandler().editSettingsMode(Integer.parseInt(DMT.getText()), mainControl.getModes().get(0).getId());
            } catch (SQLException s) {
                s.printStackTrace();
            }
            mainControl.reloadSettings();
        }
    }

    public void saveTimeSettings(){
        if(startTime.canRetrieveTime() && endTime.canRetrieveTime()) {

            int dayStart = startDay.getItems().indexOf(startDay.getValue()) + 1;
            int dayEnd = endDay.getItems().indexOf(endDay.getValue()) + 1;

            if (dayEnd < dayStart) {
                Alert wrongOrderAlert = new Alert(Alert.AlertType.INFORMATION, "Start day must be before end day.");
                wrongOrderAlert.show();
            } else {
                try {

                    LocalTime start = startTime.getLocalTimeValue();
                    LocalTime end = endTime.getLocalTimeValue();

                    TimeSettings settings = null;
                    for (TimeSettings currentSettings : mainControl.getTimeSettings()) {
                        if (dayStart >= currentSettings.getStartDay() && dayStart <= currentSettings.getEndDay()
                                || dayEnd >= currentSettings.getStartDay() && dayEnd <= currentSettings.getEndDay()
                                || currentSettings.getEndDay() >= dayStart && currentSettings.getEndDay() <= dayEnd) {
                            settings = currentSettings;
                        }
                    }
                    if (settings != null) {
                        Alert overrideAlert = new Alert(Alert.AlertType.CONFIRMATION, "Override previous time settings?");
                        overrideAlert.showAndWait().ifPresent(choice -> {
                            if (choice == ButtonType.OK) {
                                initTimeSaving();
                            } else {
                                Alert cancelledAlert = new Alert(Alert.AlertType.INFORMATION, "Settings was not saved");
                                cancelledAlert.show();
                            }
                        });
                    } else {
                        mainControl.getDbHandler().addSettings(dayStart, dayEnd, start, end);
                    }
                } catch (SQLException s) {
                    s.printStackTrace();
                    Alert cancelledAlert = new Alert(Alert.AlertType.INFORMATION, "There was a problem putting settings into database.");
                    cancelledAlert.show();
                }
                mainControl.reloadSettings();
            }
        } else {
            System.out.println("couldn't change settings");
        }
    }

    public void initTimeSaving() {
        LocalTime start = startTime.getLocalTimeValue();
        LocalTime end = endTime.getLocalTimeValue();

        int dayStart = startDay.getItems().indexOf(startDay.getValue())+1;
        int dayEnd = endDay.getItems().indexOf(endDay.getValue())+1;

        TimeSettings settings = null;
        for(TimeSettings currentSettings : mainControl.getTimeSettings()){
            if(dayStart >= currentSettings.getStartDay() && dayStart <= currentSettings.getEndDay()
                    || dayEnd >= currentSettings.getStartDay() && dayEnd <= currentSettings.getEndDay()
                    || currentSettings.getEndDay() >= dayStart && currentSettings.getEndDay() <= dayEnd){
                settings = currentSettings;
            }
        }

        try {
            editTimeSettings(settings, dayStart, dayEnd, start, end);
        } catch (SQLException s) {
            s.printStackTrace();
            Alert cancelledAlert = new Alert(Alert.AlertType.INFORMATION, "There was a problem putting settings into database.");
            cancelledAlert.show();
        }
    }

    private void editTimeSettings(TimeSettings settings, int startDay, int endDay, LocalTime startTime, LocalTime endTime) throws SQLException {
        if(endDay-startDay > settings.getEndDay()-settings.getStartDay()){
            for(TimeSettings moreSetting: mainControl.getTimeSettings()){
                if(moreSetting != settings) {
                    if (moreSetting.getStartDay() >= startDay && moreSetting.getStartDay() <= endDay
                            && moreSetting.getEndDay() >= startDay && moreSetting.getEndDay() <= endDay) {
                        mainControl.getDbHandler().deleteTimeSetting(moreSetting.getId());
                    } else if (moreSetting.getStartDay() >= startDay && moreSetting.getStartDay() <= endDay) {
                        mainControl.getDbHandler().editSettings(endDay, moreSetting.getEndDay(), moreSetting.getStart(), moreSetting.getEnd(), moreSetting.getId());
                    } else if (moreSetting.getEndDay() >= startDay && moreSetting.getEndDay() <= endDay) {
                        mainControl.getDbHandler().editSettings(moreSetting.getStartDay(), startDay, moreSetting.getStart(), moreSetting.getEnd(), moreSetting.getId());
                    }
                }
            }
        }
        if(settings.getEndDay() > endDay) {
            mainControl.getDbHandler().addSettings(endDay + 1, settings.getEndDay(), settings.getStart(), settings.getEnd());
        }
        if(settings.getStartDay() < startDay){
            mainControl.getDbHandler().addSettings(settings.getStartDay(), startDay-1, settings.getStart(), settings.getEnd());
        }
        mainControl.getDbHandler().editSettings(startDay, endDay, startTime, endTime, settings.getId());
    }

    public void reOpen(){
        stage.hide();
        stage.show();
    }

    //FOR CLOSING
    public void close(){
        stage.close();
    }
}
