package sample;

import sample.custom_controls.CustomCell;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Created by Pål on 17.05.2017.
 */
public class RowKeeper {

    private LocalTime time;

    private ArrayList<Appointment> appointments = new ArrayList<>();
    private ArrayList<LocalDate> dates = new ArrayList<>();

    private boolean[] defaultReturnVal = {true, true, true, true, true};

    public RowKeeper(LocalTime locTime){
        this.time = locTime;
        reset();
    }

    public void addDate(int index, LocalDate date) {
        this.dates.add(index, date);
    }

    public ArrayList<Integer> getDayNums(){
        ArrayList<Integer> dayNums = new ArrayList<>();
        for(LocalDate date: dates){
            dayNums.add(date.getDayOfWeek().getValue());
        }
        return dayNums;
    }

    public boolean addAppointment(int index, Appointment appointment, boolean onTheMove){

        boolean wasFilled = false;

        if (appointments.get(index) != null) {
            wasFilled = true;
        }

        if(!wasFilled || !onTheMove) {
            appointments.remove(index);
            appointments.add(index, appointment);
        }

        return wasFilled;
    }

    public LocalDate getDate(int index) {
        return dates.get(index);
    }

    public void reset(){
        for(int i = 0; i < 5; i++){
            appointments.add(null);
            dates.add(null);
        }
    }

    public String getAppointment1(){
        //Checking if appointment is deactivated, exists or is null and returns proper corresponding value
        //only for or cell-purposes: this is how the cell finds right value
        if(defaultReturnVal[0]){
            if(appointments.get(0) != null) {
                return appointments.get(0).getName();
            }
            return null;
        }
        return CustomCell.DEACTIVATED;
    }
    public String getAppointment2(){
        if(defaultReturnVal[1]){
            if(appointments.get(1) != null) {
                return appointments.get(1).getName();
            }
            return null;
        }
        return CustomCell.DEACTIVATED;
    }
    public String getAppointment3(){
        if(defaultReturnVal[2]){
            if(appointments.get(2) != null) {
                return appointments.get(2).getName();
            }
            return null;
        }
        return CustomCell.DEACTIVATED;
    }
    public String getAppointment4(){
        if(defaultReturnVal[3]){
            if(appointments.get(3) != null) {
                return appointments.get(3).getName();
            }
            return null;
        }
        return CustomCell.DEACTIVATED;
    }
    public String getAppointment5(){
        if(defaultReturnVal[4]){
            if(appointments.get(4) != null) {
                return appointments.get(4).getName();
            }
            return null;
        }
        return CustomCell.DEACTIVATED;
    }

    public ArrayList<Appointment> getAppointments() {
        return appointments;
    }

    public LocalTime getTime(){
        return time;
    }

    public void setInactive(int index){
        defaultReturnVal[index] = false;
    }

    public boolean[] getDefaultReturnVal() {
        return defaultReturnVal;
    }

    public String getTimeString(){
        String hour = "" + time.getHour();
        String minute = "" + time.getMinute();

        if(hour.length() < 2){
            hour = "0" + hour;
        }

        if(minute.length() < 2){
            minute = "0" + minute;
        }

        return (hour + ":" + minute);
    }

}
