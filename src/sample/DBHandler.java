package sample;

import sample.controllers.Controller;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by Pål on 16.05.2017.
 */
public class DBHandler {

    private static Connection connection;
    private static boolean hasData = false;

    private int timeSettingsChanges = 0; //default value;

    private void getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        connection.setAutoCommit(true);
        initialize();
    }

    private void initialize() throws SQLException {
        if(!hasData){
            hasData = true;

            checkTableActors();
            checkTableAppointment();
            checkTableSettingsMode();
            checkTableTimeSettings();
        }
    }

    public ResultSet getDate(LocalDate date, String actor) throws SQLException, ClassNotFoundException {
        if(connection == null){
            getConnection();
        }

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM appointment WHERE actor='" + actor + "' AND date='" + date.toString() + "';");

        return result;
    }

    public ResultSet getActors() throws SQLException, ClassNotFoundException {
        if(connection == null){
            getConnection();
        }

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT actorName FROM actor;");

        return result;
    }

    public ResultSet getSettings() throws SQLException, ClassNotFoundException {
        if (connection == null){
            getConnection();
        }

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT id, startDay, endDay, startTime, endTime FROM timeSettings;");

        return result;
    }

    public ResultSet getModes() throws SQLException, ClassNotFoundException {
        if (connection == null){
            getConnection();
        }

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT id, defaultMeetTime, styleMode FROM settingsMode;");

        return result;
    }

    //FUNCTIONS FOR CHECKING IF TABLES EXIST AND MAKING & (POSSIBLY) PRE-POPULATING THEM IF NOT
    private void checkTableActors() throws SQLException{
        Statement actorStatement = connection.createStatement();
        ResultSet actorResult = actorStatement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='actor'");

        if(!actorResult.next()){

            System.out.println("Building actor table w/ pre-populated values...");

            //building table
            Statement state = connection.createStatement();
            state.execute("CREATE TABLE actor(" +
                    "id integer, " +
                    "actorName varchar(60), " +
                    "breakTime varchar(60), " +
                    "primary key(id));");

            //prepopulating
            PreparedStatement prep1 = connection.prepareStatement("INSERT INTO actor values(?,?,?);");
            prep1.setString(2, "Dr. McGregor");
            prep1.setString(3, LocalTime.of(11, 30).toString());
            prep1.execute();
            prep1.close();

            PreparedStatement prep2 = connection.prepareStatement("INSERT INTO actor values(?,?,?);");
            prep2.setString(2, "Dr. Selzzer");
            prep2.setString(3, LocalTime.of(11, 30).toString());
            prep2.execute();
            prep2.close();
        }
    }


    private void checkTableSettingsMode() throws SQLException{
        //SettingsMode table is supposed to hold application constants and stuff
        //that wouldn't be natural in any of the other tables

        Statement modeStatement = connection.createStatement();
        ResultSet modeResult = modeStatement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='settingsMode'");

        if(!modeResult.next()){
            System.out.println("Building & pre-populating settingsMode table...");

            //building table
            Statement state = connection.createStatement();
            state.execute("CREATE TABLE settingsMode(" +
                    "id integer, " +
                    "defaultMeetTime integer," +
                    "styleMode integer, " +
                    "primary key(id));");

            //prepopulating
            PreparedStatement prep = connection.prepareStatement("INSERT INTO settingsMode values(?,?,?);");
            prep.setInt(2, 30);
            prep.setInt(3, 0);
            System.out.println(prep.execute());
            prep.close();
        }
    }

    private void checkTableTimeSettings() throws SQLException{
        Statement timeStatement = connection.createStatement();
        ResultSet timeResult = timeStatement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='timeSettings'");

        if(!timeResult.next()){
            System.out.println("Building & pre-populating timeSettings table...");

            //building table
            Statement state = connection.createStatement();
            state.execute("CREATE TABLE timeSettings(" +
                    "id integer, " +
                    "startDay integer, " +
                    "endDay integer, " +
                    "startTime varchar(60), " +
                    "endTime varchar(60), " +
                    "primary key(id));");

            //prepopulating
            PreparedStatement prep = connection.prepareStatement("INSERT INTO timeSettings values(?,?,?,?,?);");
            prep.setInt(2, 1);
            prep.setInt(3, 5);
            prep.setString(4, LocalTime.of(7, 0).toString());
            prep.setString(5, LocalTime.of(17, 30).toString());
            prep.execute();
            prep.close();
        }
    }

    private void checkTableAppointment() throws SQLException{
        Statement appointStatement = connection.createStatement();
        ResultSet appointResult = appointStatement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='appointment'");

        if(!appointResult.next()){
            System.out.println("Building appointment table...");

            //building table
            Statement state = connection.createStatement();
            state.execute("CREATE TABLE appointment(" +
                    "id integer, " +
                    "date varchar(60), " +
                    "time varchar(60), " +
                    "duration integer, " + //min
                    "name varchar(60), " +
                    "actor integer, " +
                    "primary key(id));");
        }
    }

    //ADDING TO THE TABLES
    public Appointment addAppointment(Controller control, LocalDate date, Time time, int duration, String name, String actorName) throws SQLException, ClassNotFoundException {
        if(connection ==  null){
            getConnection();
        }

        PreparedStatement prep = connection.prepareStatement("INSERT INTO appointment values(?,?,?,?,?,?);");

        prep.setString(2, date.toString());
        prep.setString(3, time.toString());
        prep.setInt(4, duration);
        prep.setString(5, name);
        prep.setString(6, actorName);

        prep.execute();
        prep.close();

        Statement modeStatement = connection.createStatement();
        ResultSet lastEntry = modeStatement.executeQuery("SELECT * FROM appointment WHERE id=(SELECT MAX(id) FROM appointment)");

        return new Appointment(control, lastEntry.getInt("id"),
                lastEntry.getInt("duration"),
                lastEntry.getString("date"),
                lastEntry.getString("time"),
                lastEntry.getString("name"),
                "empty description");

    }

    public void addSettings(int startDay, int endDay, LocalTime startTime, LocalTime endTime) throws SQLException{
        timeSettingsChanges ++;

        PreparedStatement prep = connection.prepareStatement("INSERT INTO timeSettings values(?,?,?,?,?);");
        prep.setInt(2, startDay);
        prep.setInt(3, endDay);
        prep.setString(4, startTime.toString());
        prep.setString(5, endTime.toString());

        prep.execute();
        prep.close();

    }

    //EDITING THE TABLES
    public Appointment editAppointment(Controller control, String name, int id) throws SQLException{
        //TODO: should check for differences before pushing update
        PreparedStatement prep = connection.prepareStatement("UPDATE appointment SET name=? WHERE id=?");
        prep.setString(1, name);
        prep.setInt(2, id);
        prep.executeUpdate();
        prep.close();

        Statement modeStatement = connection.createStatement();
        ResultSet lastEntry = modeStatement.executeQuery("SELECT * FROM appointment WHERE id=" + id);

        return new Appointment(control, lastEntry.getInt("id"),
                lastEntry.getInt("duration"),
                lastEntry.getString("date"),
                lastEntry.getString("time"),
                lastEntry.getString("name"), "empty description");

    }

    public void editSettings(int startDay, int endDay, LocalTime startTime, LocalTime endTime, int id) throws SQLException{
        //TODO: should check for differences before pushing update
        timeSettingsChanges ++;

        PreparedStatement prep = connection.prepareStatement("UPDATE timeSettings SET startDay=?, endDay=?, startTime=?, endTime=? WHERE id=?");
        prep.setInt(1, startDay);
        prep.setInt(2, endDay);
        prep.setString(3, startTime.toString());
        prep.setString(4, endTime.toString());
        prep.setInt(5, id);
        System.out.println(prep.executeUpdate());
        prep.close();
    }

    public void editSettingsMode(int meetTime, int id) throws SQLException{
        //TODO: should check for differences before pushing update
        timeSettingsChanges ++;

        PreparedStatement prep = connection.prepareStatement("UPDATE settingsMode SET defaultMeetTime=? WHERE id=?");
        prep.setInt(1, meetTime);
        prep.setInt(2, id);
        prep.executeUpdate();
        prep.close();
    }

    //DELETING STUFF FROM TABLES
    public void deleteAppointment(int id) throws SQLException{
        PreparedStatement prep = connection.prepareStatement("DELETE FROM appointment WHERE id=?");
        prep.setInt(1, id);
        prep.executeUpdate();
        prep.close();
    }

    public void deleteTimeSetting(int id) throws SQLException{
        timeSettingsChanges ++;

        PreparedStatement prep = connection.prepareStatement("DELETE FROM timeSettings WHERE id=?");
        prep.setInt(1, id);
        prep.executeUpdate();
        prep.close();
    }


    //FOR CHANGE-CHECKING
    public int getTimeSettingsChanges() {
        return timeSettingsChanges;
    }

    //Making sure the database-connection is not held after the program is closed
    public void shutDown(){
        System.out.println("Shutting down Database");
        try {
            connection.close();
        } catch (SQLException s){
            s.printStackTrace();
        }
    }
}
