package sample;

/**
 * Created by Pål on 05.06.2017.
 */
public class Mode {

    //will probably include more stuff later
    private int defaultMeetTime;
    private int styleMode;
    private int id;

    public Mode(int defaultMeetTime, int mode, int id){
        this.defaultMeetTime = defaultMeetTime;
        this.styleMode = mode;
        this.id = id;
    }

    public int getStyleMode() {
        return styleMode;
    }

    public int getDefaultMeetTime() {
        return defaultMeetTime;
    }

    public int getId() {
        return id;
    }
}
