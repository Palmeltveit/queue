package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableCell;
import sample.controllers.Controller;
import sample.custom_controls.CustomCell;
import sample.custom_controls.CustomColumn;
import sample.custom_controls.CustomTable;

import java.sql.Time;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

/**
 * Created by Pål on 17.05.2017.
 */
public class Appointment {

    private int DbID;
    private int duration;
    private LocalDate date;
    private LocalTime time;
    private SimpleStringProperty name;
    private String description;
    private Controller mainControl;

    private boolean isSelected = false;

    private String type = "filledCell";

    public Appointment(Controller mainControl, int dbID, int duration, String date, String time, String name, String description){

        this.time = LocalTime.parse(time);

        this.name = new SimpleStringProperty(name);
        this.description = description;

        this.DbID = dbID;
        this.duration = duration;

        this.mainControl = mainControl;

        try {
            //converting date string to LocalTime
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            this.date = format.parse(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e){
            e.printStackTrace();
        }
    }

    public void setSelected(boolean selected, CustomCell cell){
        this.isSelected = selected;

        CustomColumn col = (CustomColumn) cell.getTableColumn();
        if(selected) {
            col.selectAptCells(time, duration);
        } else {
            col.removeSelected();
        }
    }

    public void moveStartTime(int howMuch){
        time = time.plusMinutes(howMuch*mainControl.getDefaultMeetTime());
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public int getDbID(){
        return DbID;
    }

    public LocalTime getTime() {
        return time;
    }

    public LocalDate getDate(){
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void deactivate(){
        this.name.set(CustomCell.DEACTIVATED);
    }

    public String getName(){
        return name.get();
    }

    @Override
    public String toString() {
        return name.get();
    }
}
