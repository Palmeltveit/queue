package sample;

import java.time.LocalTime;

/**
 * Created by Pål on 29.05.2017.
 */
public class TimeSettings {

    private int startDay;
    private int endDay;

    private LocalTime start;
    private LocalTime end;

    private int id;

    public TimeSettings(int startDay, int endDay, LocalTime start, LocalTime end, int id){
        this.startDay = startDay;
        this.endDay = endDay;

        this.start = start;
        this.end = end;

        this.id = id;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public int getStartDay() {
        return startDay;
    }

    public int getEndDay() {
        return endDay;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "start: " + start + ", end: " + end;
    }
}
