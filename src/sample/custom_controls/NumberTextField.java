package sample.custom_controls;

import javafx.scene.control.TextField;

/**
 * Created by Pål on 16.05.2017.
 */
public class NumberTextField extends TextField{

    private int maxLength = 1;
    private int highestNum = 5;

    @Override
    public void replaceText(int start, int end, String text)
    {
        if (validate(text))
        {
            super.replaceText(start, end, text);
            verify();
        }
    }

    public void setMaxLength(int newMax){
        this.maxLength = newMax;
    }

    public void setHighestNumber(int highest){
        this.highestNum = highest;
    }

    @Override
    public void replaceSelection(String text)
    {
        if (validate(text))
        {
            super.replaceSelection(text);
            verify();
        }
    }

    private boolean validate(String text)
    {
        return text.matches("[0-"+highestNum+"]*");
    }

    private void verify() {
        if (getText().length() > maxLength) {
            setText(getText().substring(0, maxLength));
        }

    }
}
