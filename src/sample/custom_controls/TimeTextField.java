package sample.custom_controls;

import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * Created by Pål on 29.05.2017.
 */
public class TimeTextField extends TextField {

    private int maxLength = 5;

    public TimeTextField(){
        this.setPromptText("hh:mm");
    }

    @Override
    public void replaceText(int start, int end, String text){
        if (validate(text)){
            if(text.length() > 0) {
                if (getText().length() == 0 && Integer.parseInt(text) > 2) {
                    text = ("0" + text);
                } else if(getText().length() == 3 && Integer.parseInt(text) >= 6){
                    text = ("0" + text);
                } else if(getText().length() == 1 && Integer.parseInt(getText().substring(0,1)) == 2 && Integer.parseInt(text) >= 4){
                    text = "";
                }
                if (getText().length() + text.length() == 2) {
                    text += ":";
                }
            }
            super.replaceText(start, end, text);
            verify();
        }
    }

    @Override
    public void replaceSelection(String text){
        if (validate(text))
        {
            super.replaceSelection(text);
            verify();
        }
    }

    private boolean validate(String text){
        return (text.matches("[:0-9]*") && getText().length() < maxLength) || !(text.length() > 0);
    }

    private void verify(){
        if (getText().length() > maxLength) {
            setText(getText().substring(0, maxLength));
        }
    }

    public boolean canRetrieveTime(){
        return getText().length() == 5;
    }

    public LocalTime getLocalTimeValue(){
        return LocalTime.of(Integer.parseInt(getText().substring(0, 2)), Integer.parseInt(getText().substring(3, 5)));
    }
}
