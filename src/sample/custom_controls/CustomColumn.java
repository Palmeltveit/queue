package sample.custom_controls;

import javafx.application.Platform;
import javafx.scene.control.TableColumn;
import sample.Appointment;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * Created by Pål on 22.06.2017.
 */
public class CustomColumn extends TableColumn {

    private ArrayList<CustomCell> cells = new ArrayList<>();

    private int lastCellIndex;

    private int sCellIndex;
    private int eCellIndex;

    private int mSCellIndex;
    private int mECellIndex;

    private Appointment collidingApt;

    private int collidingAptStart;
    private int collidingAptEnd;

    private int sBCollide;
    private int eBCollide;

    private boolean canMove = true;

    public void addCell(CustomCell cell){
        if(!cells.contains(cell)) {
            cells.add(cell);
        }
    }

    public boolean moveAptToCol(Appointment apt, CustomCell cell, CustomColumn col){
        CustomTable table = (CustomTable) getTableView();
        int rowAptIndex = table.getColumns().indexOf(this) - 1;

        mSCellIndex = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
        mECellIndex = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime().plusMinutes(apt.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime() - 1;

        if(col.checkMovability(mSCellIndex, mECellIndex)) {
            for (int i = mSCellIndex; i <= mECellIndex; i++) {
                table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, null, false);
            }

            for (CustomCell c : cells) {
                if (c.getIndex() >= mSCellIndex && c.getIndex() <= mECellIndex) {
                    c.removeStyleClasses();
                    c.setText(null);
                }
            }

            col.addAppointment(apt, cell, mSCellIndex, mECellIndex);

            return true;
        }
        return false;
    }

    public boolean checkMovability(int sCellIndex, int eCellIndex){
        CustomTable table = (CustomTable) getTableView();
        int rowAptIndex = table.getColumns().indexOf(this) - 1;

        boolean movable = true;

        for(int i = sCellIndex; i <= eCellIndex; i++){
            if(table.getMainControl().getRows().get(i).getAppointments().get(rowAptIndex) != null){
                movable = false;
            }
        }

        return movable;
    }

    public void addAppointment(Appointment apt, CustomCell cell, int sCellIndex, int eCellIndex){
        CustomTable table = (CustomTable) getTableView();
        int rowAptIndex = table.getColumns().indexOf(this) - 1;

        lastCellIndex = cell.getIndex();

        for(int i = sCellIndex; i <= eCellIndex; i++){
            table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, apt, false);
        }

        for (CustomCell c : cells) {
            if (c.getIndex() >= sCellIndex && c.getIndex() <= eCellIndex) {
                if (sCellIndex == eCellIndex) {
                    c.setAptPlace(0);
                    c.setText(apt.getName());
                } else if (c.getIndex() == sCellIndex) {
                    c.setAptPlace(1);
                    c.setText(apt.getName());
                } else {
                    if (c.getIndex() == eCellIndex) {
                        c.setAptPlace(3);
                    } else {
                        c.setAptPlace(2);
                    }
                    c.setText(null);
                }
                c.select();
            }
        }
    }

    public void moveAptCells(Appointment apt, CustomCell cell){
        CustomTable table = (CustomTable) getTableView();
        int rowAptIndex = table.getColumns().indexOf(this) - 1;

        if(!table.isMoving()){
            lastCellIndex = cell.getIndex();
        } else {
            int diff = cell.getIndex() - lastCellIndex;

            apt.moveStartTime(diff);
            mSCellIndex = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
            mECellIndex = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime().plusMinutes(apt.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime() - 1;

            int removedCellIndex = -1;

            if (diff > 0) {

                if (table.getMainControl().getRows().get(mECellIndex).addAppointment(rowAptIndex, apt, true)) {

                    apt.moveStartTime(-diff);
                    Appointment colliding = table.getMainControl().getRows().get(mECellIndex).getAppointments().get(rowAptIndex);

                    if (collidingApt != colliding) {
                        collidingApt = colliding;

                        collidingAptStart = (int) table.getMainControl().getSumTimeSettings().getStart().until(colliding.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
                        collidingAptEnd = (int) (table.getMainControl().getSumTimeSettings().getStart().until(colliding.getTime().plusMinutes(colliding.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime()) - 1;

                        sBCollide = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
                        eBCollide = (int) (table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime().plusMinutes(apt.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime()) - 1;
                    }

                    canMove = false;
                } else if((mSCellIndex >= collidingAptStart && mSCellIndex <= collidingAptEnd) ||
                        (collidingAptStart >= mSCellIndex && collidingAptStart <= mECellIndex) ||
                        (collidingAptEnd >= mSCellIndex && collidingAptEnd <= mECellIndex)) {

                    if(table.getMainControl().getRows().get(mECellIndex).getAppointments().get(rowAptIndex) == apt) {
                        table.getMainControl().getRows().get(mECellIndex).addAppointment(rowAptIndex, null, false);
                    }

                    apt.moveStartTime(-diff);
                    canMove = false;
                } else {

                    if(table.getMainControl().getRows().get(mSCellIndex - 1).getAppointments().get(rowAptIndex) == apt) {
                        table.getMainControl().getRows().get(mSCellIndex - 1).addAppointment(rowAptIndex, null, false);
                    }

                    if(!canMove){
                        if(sBCollide != mSCellIndex) {
                            for (int i = sBCollide; i <= eBCollide; i++) {
                                table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, null, false);
                            }
                            for (int i = mSCellIndex; i <= mECellIndex; i++) {
                                table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, apt, false);
                            }
                        }
                        canMove = true;
                        collidingApt = null;
                    } else {
                        removedCellIndex = mSCellIndex - 1;
                    }

                    lastCellIndex = cell.getIndex();
                }

            } else if (diff < 0) {

                if (table.getMainControl().getRows().get(mSCellIndex).addAppointment(rowAptIndex, apt, true)) {

                    apt.moveStartTime(-diff);
                    Appointment colliding = table.getMainControl().getRows().get(mSCellIndex).getAppointments().get(rowAptIndex);

                    if (collidingApt != colliding) {
                        collidingApt = colliding;

                        collidingAptStart = (int) table.getMainControl().getSumTimeSettings().getStart().until(colliding.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
                        collidingAptEnd = (int) (table.getMainControl().getSumTimeSettings().getStart().until(colliding.getTime().plusMinutes(colliding.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime()) - 1;

                        sBCollide = (int) table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime(), MINUTES) / table.getMainControl().getDefaultMeetTime();
                        eBCollide = (int) (table.getMainControl().getSumTimeSettings().getStart().until(apt.getTime().plusMinutes(apt.getDuration()), MINUTES) / table.getMainControl().getDefaultMeetTime()) - 1;
                    }

                    canMove = false;
                } else if ((mECellIndex >= collidingAptStart && mECellIndex <= collidingAptEnd) ||
                        (collidingAptStart >= mSCellIndex && collidingAptStart <= mECellIndex) ||
                        (collidingAptEnd >= mSCellIndex && collidingAptEnd <= mECellIndex)) {

                    if(table.getMainControl().getRows().get(mSCellIndex).getAppointments().get(rowAptIndex) == apt) {
                        table.getMainControl().getRows().get(mSCellIndex).addAppointment(rowAptIndex, null, false);
                    }

                    apt.moveStartTime(-diff);
                    canMove = false;
                } else {

                    if(table.getMainControl().getRows().get(mECellIndex + 1).getAppointments().get(rowAptIndex) == apt) {
                        table.getMainControl().getRows().get(mECellIndex + 1).addAppointment(rowAptIndex, null, false);
                    }

                    if(!canMove){

                        if(sBCollide != mSCellIndex) {
                            for (int i = sBCollide; i <= eBCollide; i++) {
                                table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, null, false);
                            }
                            for (int i = mSCellIndex; i <= mECellIndex; i++) {
                                table.getMainControl().getRows().get(i).addAppointment(rowAptIndex, apt, false);
                            }
                        }
                        canMove = true;
                        collidingApt = null;
                    } else {
                        removedCellIndex = mECellIndex + 1;
                    }
                    lastCellIndex = cell.getIndex();
                }
            }

            if (canMove) {
                for (CustomCell c : cells) {
                    if (c.getIndex() >= mSCellIndex && c.getIndex() <= mECellIndex) {
                        if (mSCellIndex == mECellIndex) {
                            c.setAptPlace(0);
                            c.setText(apt.getName());
                        } else if (c.getIndex() == mSCellIndex) {
                            c.setAptPlace(1);
                            c.setText(apt.getName());
                        } else {
                            if (c.getIndex() == mECellIndex) {
                                c.setAptPlace(3);
                            } else {
                                c.setAptPlace(2);
                            }
                            c.setText(null);
                        }
                        c.select();

                    } else if (c.getIndex() == removedCellIndex || (c.getIndex() >= sBCollide && c.getIndex() <= eBCollide)) {
                        c.removeStyleClasses();
                        c.setText(null);
                    }
                }
            }
        }
    }

    public void fillVoid(int startCell, int endCell){
        for(CustomCell cell : cells){
            if(cell.getIndex() > startCell && cell.getIndex() < endCell){
                if (!cell.getStyleClass().contains("tempSelect")) {
                    CustomTable table = (CustomTable) cell.getTableView();
                    table.addCell(cell);
                    cell.setAsCurrentCell(false);
                }
            }
        }
    }

    public void selectAptCells(LocalTime time, int duration){

        Collections.sort(cells, new Comparator<CustomCell>() {
            @Override
            public int compare(CustomCell o1, CustomCell o2) {
                return o1.getIndex() - o2.getIndex();
            }
        });

        while (cells.size() > 0 && cells.get(0).getIndex() < 0){
            cells.remove(0);
        }

        if(cells.size() > 0) {

            CustomTable table = (CustomTable) cells.get(0).getTableView();

            int defaultMeetTime = table.getMainControl().getDefaultMeetTime();

            sCellIndex = (int) table.getMainControl().getSumTimeSettings().getStart().until(time, MINUTES) / defaultMeetTime;
            eCellIndex = (int) (table.getMainControl().getSumTimeSettings().getStart().until(time.plusMinutes(duration), MINUTES) / defaultMeetTime) - 1;

            for (CustomCell cell : cells) {
                if (cell.getIndex() >= sCellIndex && cell.getIndex() <= eCellIndex) {
                    cell.setType(CustomCell.selected);
                    cell.select();
                }
            }
        }
    }

    public void removeSelected(){
        for (CustomCell cell : cells) {
            if (cell.getIndex() >= sCellIndex && cell.getIndex() <= eCellIndex) {
                cell.setType(CustomCell.filled);
                cell.select();
            }
        }
        sCellIndex = cells.size();
        eCellIndex = 0;
    }

    public boolean isSelected(int index){
        return index >= sCellIndex && index <= eCellIndex;
    }

    public ArrayList<CustomCell> getCells() {
        return cells;
    }
}
