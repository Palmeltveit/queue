package sample.custom_controls;

import com.sun.javafx.scene.control.skin.DatePickerSkin;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.ResourceBundle;

/**
 * Created by Pål on 14.05.2017.
 */


import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/*
ProCalendar is a custom class made for more accurate and
user friendly interactions with the queue desktop program.

IMPORTANT FEATURES:
- always open calendar
- (ideally) drag over multiple dates to get array instead of just one
 */

public class ProCalendar extends VBox implements Initializable {
    @FXML public Pane pane;
    public Label lab;

    private LocalDate date;

    //the datepicker whose skin we are using. We can retrieve information from it.
    public DatePicker picker;

    public ProCalendar() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "pro_cal.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setValue(LocalDate newDate){
        picker.setValue(newDate);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        picker = new DatePicker(LocalDate.now());

        picker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                date = picker.getValue();

                setLabelText(date.toString()); //setting the date to label
            }
        });

        DatePickerSkin datePickerSkin = new DatePickerSkin(picker);
        Node calendar = datePickerSkin.getPopupContent();

        pane.getChildren().add(calendar);
    }


    public void setLabelText(String text){
        lab.setText(text);
    }
}