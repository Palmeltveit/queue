package sample.custom_controls;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Cell;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import sample.Appointment;
import sample.RowKeeper;
import sample.controllers.Controller;

import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Pål on 12.06.2017.
 */
public class CustomCell extends TableCell<String, String> {

    public static String DEACTIVATED = "_INACTIVE_";

    public static String filled = "filledCell";
    public static String selected = "tempSelect";

    private boolean hasValue = false;
    private boolean isCurrentCell = false;
    private boolean alreadyEntered = false;

    private String type = filled;
    private String[] place = {"Single", "Top", "", "Bottom"};

    private Appointment correspondingApt;

    public static String[] styleClasses = {"tempSelect", "tempSelectTop", "tempSelectBottom", "tempSelectSingle", "deactivated_cell", "filledCell", "filledCellTop", "filledCellBottom", "filledCellSingle"};

    private int aptPlace = 1; // 0 = single, 1 = top, 2 = middle, 3 = bottom.

    private CustomTable table;
    public CustomCell(CustomTable table){

        this.table = table;
        int prefHeight = 100/(60/table.getMainControl().getDefaultMeetTime());

        setStyle("-fx-pref-height: " + prefHeight + ";");

        setOnMouseClicked(event -> {
            if(hasValue && !getStyleClass().contains("deactivated_cell")){
                if(correspondingApt != null) {
                    table.selectAppointment(correspondingApt, this);
                }
            }
        });

        setOnMouseDragOver(event -> {
            if(!isCurrentCell && !alreadyEntered) {
                if(!table.isSelecting() && hasValue || table.isMoving()){
                    table.moveCell(this);
                } else if(!table.isMoving()) {
                    table.addCell(this);
                    System.out.println("adding cell");
                }

                alreadyEntered = true;
            }
        });

        setOnMouseDragExited(event -> {
            if(!hasValue || getItem() == null) {
                isCurrentCell = false;
            }
            alreadyEntered = false;
        });
    }

    @Override
    public void updateIndex(int i) {
        super.updateIndex(i);
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        removeStyleClasses();

        if(!getStyleClass().contains("custom_cell")){
            getStyleClass().add("custom_cell");
        }

        if(item == null) {
            setText(null);
            hasValue = false;
            correspondingApt = null;
            removeStyleClasses();
        } else {
            hasValue = true;

            if(item.equals(DEACTIVATED)){
                getStyleClass().add("deactivated_cell");
                setText(null);
            } else {
                correspondingApt = table.getMainControl().getRows().get(getIndex()).getAppointments().get(table.getColumns().indexOf(getTableColumn()) - 1);

                LocalTime correspondingAptTime = correspondingApt.getTime();
                LocalTime correspondingTime = table.getMainControl().getSumTimeSettings().getStart().plusMinutes(getIndex()*table.getMainControl().getDefaultMeetTime());

                CustomColumn col = (CustomColumn) getTableColumn();
                type = col.isSelected(getIndex()) ? selected : filled;
                if(type.equals(selected)){
                    System.out.println("selected: " + getIndex());
                }

                if(correspondingAptTime.equals(correspondingTime) && correspondingApt.getDuration() == table.getMainControl().getDefaultMeetTime() && !getStyleClass().contains(type + "Single")){
                    aptPlace = 0;
                } else if(correspondingAptTime.equals(correspondingTime) && !getStyleClass().contains(type + "Top")){
                    aptPlace = 1;
                } else if(correspondingAptTime.plusMinutes(correspondingApt.getDuration()-table.getMainControl().getDefaultMeetTime()).equals(correspondingTime) && !getStyleClass().contains(type + "Bottom")){
                    aptPlace = 3;
                    item = null;
                } else if(!getStyleClass().contains(type)){
                    aptPlace = 2;
                    item = null;
                }

                select();
                setText(item);
            }
        }
    }

    public void select(){
        removeStyleClasses();
        if(!getStyleClass().contains(type + place[aptPlace])) { //should never contain this after removing styleClasses, but still good practice
            getStyleClass().add(type + place[aptPlace]);
        }
    }

    public void setAptPlace(int aptPlace){
        this.aptPlace = aptPlace;
        select();
    }

    public void setType(String type){
        this.type = type;
        System.out.println(this.type);
    }

    public String getType() {
        return type;
    }

    public void removeStyleClasses(){
        for(String styleClass : styleClasses){
            if(getStyleClass().contains(styleClass)){
                getStyleClass().remove(styleClass);
            }
        }
    }

    public void setAsCurrentCell(boolean isCurrentCell){
        this.isCurrentCell = isCurrentCell;
    }

    public boolean isCurrentCell() {
        return isCurrentCell;
    }

    public boolean hasValue() {
        return hasValue;
    }
}
