package sample.custom_controls;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import sample.Appointment;
import sample.controllers.Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Pål on 14.06.2017.
 */
public class CustomTable extends TableView {

    private boolean lastIncreased = false;

    private boolean isSelecting = false;
    private boolean isMoving = false;

    private int lastAcceptable = -1;

    //for apt-moving
    private Appointment movingApt;
    private CustomColumn movingCol;

    private ArrayList<CustomCell> selectedCells = new ArrayList<>();

    private Controller mainControl;

    private Appointment lastSelectedAppointment;
    private CustomCell lastSelectedCell;

    public CustomTable(){

        this.skinProperty().addListener((obs, oldSkin, newSkin) -> {
            final TableHeaderRow header = (TableHeaderRow) lookup("TableHeaderRow");
            header.reorderingProperty().addListener((o, oldVal, newVal) -> header.setReordering(false));
        });

        setOnDragEntered(event -> {
            startFullDrag();
        });

        setOnMouseDragExited(event -> {
            if(selectedCells.size() > 0) {
                if(selectedCells.size() > 1 || lastIncreased) {
                    mainControl.openTableForm(selectedCells.size(), selectedCells.get(0).getIndex());
                } else {
                    refresh();
                }
                selectedCells = new ArrayList<>();
            }
            movingApt = null;
            isSelecting = false;
            isMoving = false;
            System.out.println("stopped");
        });
    }
    public void setMainControl(Controller control){
        this.mainControl = control;
    }

    public Controller getMainControl() {
        return mainControl;
    }

    public void moveCell(CustomCell cell){
        CustomColumn col = (CustomColumn) cell.getTableColumn();

        boolean justStarted = false;

        if(movingApt == null || !isMoving) {
            movingApt = mainControl.getRows().get(cell.getIndex()).getAppointments().get(getColumns().indexOf(col) - 1);
            justStarted = true;
        }

        if(col == movingCol || movingCol == null || justStarted) {
            col.moveAptCells(movingApt, cell);
            movingCol = col;
        } else if(movingCol.moveAptToCol(movingApt, cell, col)){
            movingCol = col;
        }
        isMoving = true;

    }

    public void addCell(CustomCell cell) {

        isSelecting = true;

        if (selectedCells.size() == 0 || cell.getTableColumn() == selectedCells.get(0).getTableColumn()) {

            if (!cell.hasValue() && (cell.getIndex() < lastAcceptable || lastAcceptable < 0)) {
                if (selectedCells.contains(cell)) {
                    while (selectedCells.get(selectedCells.size() - 1) != cell) {
                        selectedCells.get(selectedCells.size() - 1).removeStyleClasses();
                        selectedCells.remove(selectedCells.size() - 1);
                    }
                    lastIncreased = false;
                } else {
                    selectedCells.add(cell);
                    lastIncreased = true;
                }
                cell.setAsCurrentCell(true);
                lastAcceptable = -1;
            } else {
                if (lastAcceptable < 0) {
                    lastAcceptable = cell.getIndex();
                }
            }

            toggleCellStyles();
        }
    }

    public void selectAppointment(Appointment apt, CustomCell cell){
        if(apt != lastSelectedAppointment && lastSelectedAppointment != null) {
            lastSelectedAppointment.setSelected(false, lastSelectedCell);
        }
        lastSelectedAppointment = apt;
        lastSelectedCell = cell;
        apt.setSelected(true, cell);
    }

    public void resetSelected(){
        if(!selectedCells.get(0).hasValue()){
            for(CustomCell cell : selectedCells){
                cell.removeStyleClasses();
            }
        }
        selectedCells = new ArrayList<>();
    }

    private void toggleCellStyles(){

        if(selectedCells.size() > 0) {
            Collections.sort(selectedCells, new Comparator<CustomCell>() {
                @Override
                public int compare(CustomCell o1, CustomCell o2) {
                    return o1.getIndex() - o2.getIndex();
                }
            });

            for (int i = 1; i < selectedCells.size() - 1; i++) {
                selectedCells.get(i).setText(null);
                if (!selectedCells.get(i).getStyleClass().contains("tempSelect")) {
                    selectedCells.get(i).setType(CustomCell.selected);
                    selectedCells.get(i).setAptPlace(2);
                }
            }
            if (selectedCells.size() == 1 && !selectedCells.get(0).getStyleClass().contains("tempSelectSingle")) {
                selectedCells.get(0).setType(CustomCell.selected);
                selectedCells.get(0).setAptPlace(0);
            } else if (!selectedCells.get(0).getStyleClass().contains("tempSelectTop")) {
                selectedCells.get(0).setType(CustomCell.selected);
                selectedCells.get(0).setAptPlace(1);
            }

            selectedCells.get(selectedCells.size() - 1).setText(null);
            selectedCells.get(0).setText(mainControl.getSumTimeSettings().getStart().plusMinutes(mainControl.getDefaultMeetTime() * selectedCells.get(0).getIndex()) + " - " + mainControl.getSumTimeSettings().getStart().plusMinutes(mainControl.getDefaultMeetTime() * (selectedCells.get(selectedCells.size() - 1).getIndex() + 1)));

            if (!selectedCells.get(selectedCells.size() - 1).getStyleClass().contains("tempSelectBottom")) {
                selectedCells.get(selectedCells.size() - 1).setType(CustomCell.selected);
                selectedCells.get(selectedCells.size() - 1).setAptPlace(3);
            }

            CustomColumn col = (CustomColumn) selectedCells.get(0).getTableColumn();
            col.fillVoid(selectedCells.get(0).getIndex(), selectedCells.get(selectedCells.size()-1).getIndex());
        }
    }

    public boolean isSelecting() {
        return isSelecting;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public ArrayList<CustomCell> getSelectedCells() {
        return selectedCells;
    }

    public boolean wasLastIncreased() {
        return lastIncreased;
    }

}
