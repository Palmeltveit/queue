package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sample.controllers.Controller;
import sample.controllers.MenuBarController;

import java.io.IOException;

public class Main extends Application {

    /*root is the main content pane of the program, where we add all the different fxml views
    based on what action is needed (which view is requested by user) */
    private static BorderPane root = new BorderPane();

    public Controller control;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        //initializing the top menu bar (separate file so as to exist in all views) and setting it to root top
        FXMLLoader menuLoader = new FXMLLoader(getClass().getResource("menuBar_related/menuBar.fxml"));
        //initializing the main content, and later setting it to root center
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainNormal.fxml"));

        root.setTop(menuLoader.load());
        root.setCenter(loader.load());

        primaryStage.setTitle("Queue Desktop");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("logo1.png")));

        Scene primary = new Scene(root, 1000, 600);
        primary.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());

        primaryStage.setScene(primary);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                control.getDbHandler().shutDown();
            }
        });

        primaryStage.show();

        //updating the column width in table to match window size better
        this.control = loader.getController();
        //control.updateColumnWidth();

        //adding the main controller DBHandler to menubarController
        MenuBarController menuControl = menuLoader.getController();
        menuControl.addDBHandler(control);
    }

    public static BorderPane getRoot(){
        return root;
    }
}
